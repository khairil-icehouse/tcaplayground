//
//  MoyaProvider+Concurrency.swift
//  TCAPlayground
//
//  Created by Khairil Ushan on 11/12/21.
//

import Foundation
import Moya

enum NetworkError: Error {
	case jsonSerialization(Error)
	case underlying(Error, code: Int?, data: Data?, response: HTTPURLResponse?)
}

extension MoyaProvider {

	func requestAsync<Response>(
		_ target: Target,
		jsonDecoder: JSONDecoder
	) async throws -> Response where Response: Codable {

		try await withCheckedThrowingContinuation { continuation in

			self.request(target) { result in
				switch result {
				case let .success(response):
					do {
						let filteredResponse = try response.filterSuccessfulStatusCodes()
						let response = try jsonDecoder.decode(Response.self, from: filteredResponse.data)
						continuation.resume(returning: response)
					} catch {
						if let moyaError = error as? MoyaError {
							continuation.resume(throwing: NetworkError(moyaError: moyaError))
						} else {
							continuation.resume(throwing: NetworkError.jsonSerialization(error))
						}
					}

				case let .failure(error):
					continuation.resume(throwing: NetworkError(moyaError: error))
				}
			}
		}
	}
}

extension NetworkError {

	init(moyaError: MoyaError) {

		if let response = moyaError.response {
			self = NetworkError.underlying(
				moyaError,
				code: response.statusCode,
				data: response.data,
				response: response.response
			)
		} else {
			self = NetworkError.underlying(moyaError, code: nil, data: nil, response: nil)
		}
	}
}
