//
//  RepositoryListView+Store.swift
//  TCAPlayground
//
//  Created by Khairil Ushan on 11/12/21.
//

import Foundation
import ComposableArchitecture

typealias RepositoryListStore = Store<RepositoryListState, RepositoryListAction>
typealias RepositoryItemStore = Store<RepositoryItemState, RepositoryItemAction>

enum RepositoryListAction: Equatable {
	case searchTextFieldChanged(String)
	case searchResponse(Result<[Repository], RepositoryError>)
	case repositoryItem(index: RepositoryItemState.ID, action: RepositoryItemAction)
	case showFullScreenError(Bool)
}

enum RepositoryItemAction {
	case tapped
}

struct RepositoryListState: Equatable {
	var isLoading = false
	var repositories: IdentifiedArrayOf<RepositoryItemState> = []
	var searchQuery: String = ""
	var showFullScreenAlert = false
}

struct RepositoryItemState: Equatable, Identifiable {
	let id: Int
	let title: String
	let description: String
	let ownerName: String
	let ownerProfilePicture: URL?
}

struct RepositoryListEnvironment {
	var searchRepo: (String) -> Effect<[Repository], RepositoryError>
	var mainQueue: AnySchedulerOf<DispatchQueue>
}

struct RepositoryItemEnvironment {
	var log: (String) -> Effect<Never, Never>
}

let repositoryItemReducer = Reducer<
	RepositoryItemState,
	RepositoryItemAction,
	RepositoryItemEnvironment
> { state, action, environment in

	switch action {
	case .tapped:
		return environment.log("Tapped \(state.title)").fireAndForget()
	}
}

let repositoryListReducer = Reducer<
	RepositoryListState,
	RepositoryListAction,
	RepositoryListEnvironment
>.combine(
	repositoryItemReducer.forEach(
		state: \RepositoryListState.repositories,
		action: /RepositoryListAction.repositoryItem(index:action:),
		environment: { _ in RepositoryItemEnvironment.live }
	),
	Reducer { state, action, environment in
		switch action {
		case let .searchTextFieldChanged(keyword):
			struct SearchRepoID: Hashable {}

			state.isLoading = true
			state.searchQuery = keyword

			guard !keyword.isEmpty else {
				state.repositories = []
				state.isLoading = false
				return .cancel(id: SearchRepoID())
			}

			return environment.searchRepo(keyword)
			.debounce(id: SearchRepoID(), for: 0.3, scheduler: environment.mainQueue)
			.catchToEffect(RepositoryListAction.searchResponse)

		case let .searchResponse(.success(response)):
			state.isLoading = false
			state.repositories = IdentifiedArrayOf(uniqueElements: response.compactMap(RepositoryItemState.init))
			return .none

		case let .searchResponse(.failure(error)):
			state.repositories = []
			state.isLoading = false
			return .none

		case .repositoryItem(index: let index, action: .tapped):
			return Effect(value: .showFullScreenError(true))

		case let .showFullScreenError(show):
			state.showFullScreenAlert = show
			return .none
		}
	}
)

extension RepositoryItemState {

	init?(repository: Repository) {

		guard let id = repository.repoId else {
			return nil
		}

		self.init(
			id: id,
			title: repository.fullName ?? "-",
			description: repository.desc ?? "-",
			ownerName: repository.owner?.login ?? "-",
			ownerProfilePicture: URL(string: repository.owner?.avatarUrl ?? "")
		)
	}
}

extension RepositoryListEnvironment {

	static let live = Self(
		searchRepo: { keyword in
			Effect<[Repository], Error>.task {
				try await GithubRepoManager.live().load(keyword)
			}
			.mapError { $0 as! RepositoryError }
			.eraseToEffect()
		},
		mainQueue: .main
	)
}

extension RepositoryItemEnvironment {

	static let live = Self(
		log: { data in
			.fireAndForget {
				print(data)
			}
		}
	)
}
