//
//  RepositoryListView.swift
//  TCAPlayground
//
//  Created by Khairil Ushan on 10/12/21.
//

import Combine
import ComposableArchitecture
import SwiftUI

struct RepositoryListView: View {

	let store: RepositoryListStore

	var body: some View {
		WithViewStore(store) { viewStore in
			NavigationView {
				VStack {
					SearchTextField(
						text: viewStore.binding(get: \.searchQuery, send: RepositoryListAction.searchTextFieldChanged)
					)
					if viewStore.isLoading {
						ProgressView("Searching \(viewStore.searchQuery) ...")
							.frame(maxHeight: .infinity)
					} else {
						List {
							ForEachStore(
								self.store.scope(
									state: \.repositories,
									action: RepositoryListAction.repositoryItem(index:action:)
								),
								content: RepositoryItemView.init
							)
						}
						.listStyle(.plain)
						.ignoresSafeArea()
					}
				}
				.navigationTitle("Search Github Repository")
				.fullScreenCover(
					isPresented: viewStore.binding(
						get: \.showFullScreenAlert,
						send: RepositoryListAction.showFullScreenError
					),
					onDismiss: nil
				) {
					Text("Full Screen")
						.onTapGesture {
							viewStore.send(.showFullScreenError(false))
						}
				}
			}
		}
	}
}

struct SearchTextField: View {

	@Binding var text: String

	var body: some View {

		TextField("Search repository name", text: $text)
			.textFieldStyle(.roundedBorder)
			.autocapitalization(.none)
			.disableAutocorrection(true)
			.padding(.init(top: 16, leading: 16, bottom: 8, trailing: 16))
	}
}

struct RepositoryItemView: View {

	let store: Store<RepositoryItemState, RepositoryItemAction>

	var body: some View {
		WithViewStore(store) { viewStore in
			HStack(alignment: .top) {

				AsyncImage(url: viewStore.ownerProfilePicture) { image in
					image
						.resizable()
						.scaledToFill()
				} placeholder: {
					Color.blue.opacity(0.4)
				}
				.frame(width: 40, height: 40)
				.cornerRadius(4)

				VStack(alignment: .leading) {
					Text(viewStore.title)
						.font(.headline)
					Text("By ")
						.font(.subheadline)
						.foregroundColor(.gray) +
					Text(viewStore.ownerName)
						.font(.subheadline)
						.foregroundColor(.gray)
					Spacer(minLength: 10)
					Text(viewStore.description)
						.font(.body)
				}
			}
			.onTapGesture {
				viewStore.send(.tapped)
			}
		}
	}
}

struct RepositoryListView_Previews: PreviewProvider {

	static var previews: some View {

		RepositoryListView(
			store: .init(
				initialState: .init(isLoading: false, repositories: []),
				reducer: repositoryListReducer,
				environment: .live
			)
		)
	}
}
