//
//  TCAPlaygroundApp.swift
//  TCAPlayground
//
//  Created by Khairil Ushan on 10/12/21.
//

import ComposableArchitecture
import SwiftUI

@main
struct TCAPlaygroundApp: App {

	let store = AppStore(
		initialState: AppState(),
		reducer: appReducer,
		environment: AppEnvironment.live
	)

	var body: some Scene {
		WindowGroup {
			RepositoryListView(
				store: store.scope(state: \.repositoryList, action: AppAction.repositoryList(action:))
			)
		}
	}
}

typealias AppStore = Store<AppState, AppAction>

struct AppState {
	var repositoryList = RepositoryListState()
}

enum AppAction {
	case repositoryList(action: RepositoryListAction)
}

struct AppEnvironment {
	let searchRepo: (String) -> Effect<[Repository], RepositoryError>
	let log: (String) -> Effect<Never, Never>
	let mainQueue: AnySchedulerOf<DispatchQueue>
}

extension AppEnvironment {

	static let live = Self(
		searchRepo: { keyword in
			Effect<[Repository], Error>.task {
				try await GithubRepoManager.live().load(keyword)
			}
			.mapError { $0 as! RepositoryError }
			.eraseToEffect()
		},
		log: { data in
			.fireAndForget {
				print(data)
			}
		},
		mainQueue: .main
	)

	var repositoryList: RepositoryListEnvironment {
		.init(searchRepo: searchRepo, mainQueue: mainQueue)
	}
}

let appReducer = Reducer<AppState, AppAction, AppEnvironment>.combine(
	repositoryListReducer.pullback(
		state: \.repositoryList,
		action: /AppAction.repositoryList(action:),
		environment: \.repositoryList
	),
	Reducer { state, action, environment in
		switch action {
		case let .repositoryList(repositoryAction):
			return .none
		}
	}
)
