//
//  Networking+SearchRepositories.swift
//  TCAPlayground
//
//  Created by Khairil Ushan on 11/12/21.
//

import Foundation
import Moya

extension Networking where Parameter == String, Response == SearchRepositoryResponse {

	static let searchRepositories = Networking { keyword in
		let provider = MoyaProvider<GithubEndpoint>()
		let decoder = JSONDecoder()
		return try await provider.requestAsync(.searchRepositories(keyword: keyword), jsonDecoder: decoder)
	}
}
