//
//  Repository.swift
//  TCAPlayground
//
//  Created by Khairil Ushan on 10/12/21.
//

import Foundation

struct Repository: Codable, Equatable {

	enum CodingKeys: String, CodingKey {
		case repoId = "id"
		case name
		case fullName = "full_name"
		case desc = "description"
		case owner
		case htmlUrl = "html_url"
	}

	let repoId: Int?
	let name: String?
	let fullName: String?
	let desc: String?
	let htmlUrl: String?
	let owner: Owner?
}

extension Repository {

	struct Owner: Codable, Equatable {

		enum CodingKeys: String, CodingKey {
			case userId = "id"
			case login
			case avatarUrl = "avatar_url"
			case htmlUrl = "html_url"
		}

		let userId: Int?
		let login: String?
		let avatarUrl: String?
		let htmlUrl: String?
	}
}
