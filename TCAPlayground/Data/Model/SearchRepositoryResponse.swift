//
//  SearchRepositoryResponse.swift
//  TCAPlayground
//
//  Created by Khairil Ushan on 10/12/21.
//

import Foundation

struct SearchRepositoryResponse: Codable {

	enum CodingKeys: String, CodingKey {
		case totalCount = "total_count"
		case incompleteResult = "incomplete_result"
		case items
	}

	let totalCount: Int?
	let incompleteResult: Bool?
	let items: [Repository]?
}
