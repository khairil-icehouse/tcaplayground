//
//  RepositoryError.swift
//  TCAPlayground
//
//  Created by Khairil Ushan on 17/12/21.
//

import Foundation

enum RepositoryError: Swift.Error, Equatable {
	case unauthorized
}
