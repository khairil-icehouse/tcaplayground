//
//  StoredRepository.swift
//  TCAPlayground
//
//  Created by Khairil Ushan on 11/12/21.
//

import Foundation

struct StoredRepository: Codable {
	var keyword: String
	var repositories: [Repository]
}
