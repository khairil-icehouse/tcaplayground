//
//  Storage.swift
//  TCAPlayground
//
//  Created by Khairil Ushan on 11/12/21.
//

import Foundation

public struct Storage<Element> {
	var load: () throws -> Element?
	var save: (_ object: Element) throws -> Void
	var removeAllData: () throws -> Void
}

public extension Storage {
	static var inMemory: Self {
		var cached: Element?
		return .init(
			load: { cached },
			save: { cached = $0 },
			removeAllData: { cached = nil }
		)
	}
}

public extension Storage where Element: Codable {
	static func filename(_ filename: String, jsonDecoder: JSONDecoder, jsonEncoder: JSONEncoder) -> Self {

		let storageURL = storageURL(filename: filename)

		return .init(
			load: {
				guard FileManager.default.fileExists(atPath: storageURL.path) else { return nil }
				let data = try Data(contentsOf: storageURL)
				return try jsonDecoder.decode(Element.self, from: data)
			},
			save: { object in
				try ensureDirectoryExists(storageURL: storageURL)
				let data = try jsonEncoder.encode(object)
				try data.write(to: storageURL)
			},
			removeAllData: {
				try FileManager.default.removeItem(at: storageURL)
			})
	}

	private static func storageURL(filename: String) -> URL {
		let appSupport = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true).first!
		return URL(fileURLWithPath: appSupport).appendingPathComponent(filename)
	}

	private static func ensureDirectoryExists(storageURL: URL) throws {
		// ensure directory exists
		let dir = storageURL.deletingLastPathComponent()

		if !FileManager.default.fileExists(atPath: dir.path) {
			try FileManager.default.createDirectory(at: dir, withIntermediateDirectories: true, attributes: nil)
		}
	}
}
