//
//  Networking.swift
//  TCAPlayground
//
//  Created by Khairil Ushan on 02/01/22.
//

import Foundation
import Moya

struct Networking<Parameter, Response> {
	let request: (Parameter) async throws -> Response
}
