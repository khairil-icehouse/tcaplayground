//
//  GithubRepoManager.swift
//  TCAPlayground
//
//  Created by Khairil Ushan on 11/12/21.
//

import Foundation
import ComposableArchitecture

struct GithubRepoManager {
	var load: (String) async throws -> [Repository]
}

extension GithubRepoManager {

	static var jsonDecoder: JSONDecoder {
		let decoder = JSONDecoder()
		decoder.dateDecodingStrategy = .iso8601
		return decoder
	}

	static var jsonEncoder: JSONEncoder {
		let encoder = JSONEncoder()
		encoder.dateEncodingStrategy = .iso8601
		return encoder
	}

	static func live(
		loader: Networking<String, SearchRepositoryResponse> = .searchRepositories,
		storage: Storage<[StoredRepository]> = .filename(
			"github_repo.json",
			jsonDecoder: jsonDecoder,
			jsonEncoder: jsonEncoder
		)
	) -> Self {

		.init { keyword in

			var storedRepos = (try? storage.load()) ?? []

			do {
				let searchResult = try await loader.request(keyword).items ?? []

				if searchResult.isEmpty {
					return searchResult
				}

				let inCache = storedRepos.first(where: { $0.keyword == keyword }) != nil
				if inCache {
					storedRepos = storedRepos.map { storedRepo in
						if storedRepo.keyword == keyword {
							return StoredRepository(keyword: keyword, repositories: searchResult)
						}
						return storedRepo
					}
				} else {
					storedRepos.append(.init(keyword: keyword, repositories: searchResult))
				}

				try? storage.save(storedRepos)

				return searchResult
			} catch {
				let storedReposByKeyword = storedRepos
					.first(where: { $0.keyword == keyword })
					.map(\.repositories)
				if let repos = storedReposByKeyword, !repos.isEmpty {
					return repos
				}
				throw error
			}
		}
	}
}
