//
//  GithubEndpoint.swift
//  TCAPlayground
//
//  Created by Khairil Ushan on 10/12/21.
//

import Foundation
import Moya

enum GithubEndpoint {
	case searchRepositories(keyword: String)
}

extension GithubEndpoint: TargetType {

	var baseURL: URL {
		URL(string: "https://api.github.com")!
	}

	var path: String {
		switch self {
		case .searchRepositories:
			return "/search/repositories"
		}
	}

	var method: Moya.Method {
		.get
	}

	var parameters: [String: Any] {
		switch self {
		case let .searchRepositories(keyword):
			return [
				"q": keyword
			]
		}
	}

	var parameterEncoding: ParameterEncoding {
		URLEncoding.default
	}

	var task: Task {
		.requestParameters(parameters: parameters, encoding: parameterEncoding)
	}

	var headers: [String : String]? {
		[:]
	}
}
