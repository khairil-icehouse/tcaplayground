//
//  GithubRepoManagerTest.swift
//  TCAPlaygroundTests
//
//  Created by Khairil Ushan on 17/12/21.
//

import XCTest
@testable import TCAPlayground

final class GithubRepoManagerTest: XCTestCase {

	func testGetingResultFromNetwork() async throws {

		let mockRepo = [ Repository(repoId: 1, name: "test", fullName: "test", desc: "test", htmlUrl: "test", owner: nil) ]
		let manager = GithubRepoManager.live(
			loader: .init(request: { _ in
				SearchRepositoryResponse(
					totalCount: 1,
					incompleteResult: false,
					items: mockRepo
				)
			}),
			storage: .inMemory
		)

		let result = try await manager.load("test")

		XCTAssertEqual(result, mockRepo)
	}

	func testGetingResultFromNetworkAndCacheIt() async throws {

		let mockRepo = [ Repository(repoId: 1, name: "test", fullName: "test", desc: "test", htmlUrl: "test", owner: nil) ]
		let cache = Storage<[StoredRepository]>.inMemory
		let manager = GithubRepoManager.live(
			loader: .init(request: { _ in
				SearchRepositoryResponse(
					totalCount: 1,
					incompleteResult: false,
					items: mockRepo
				)
			}),
			storage: cache
		)

		_ = try await manager.load("test")

		let cachedRepo = try cache.load()?.first(where: { $0.keyword == "test" }).map(\.repositories)

		XCTAssertEqual(cachedRepo, mockRepo)
	}

	func testGettingResultsFromCacheWhenNetworkFail() async throws {

		let mockRepo = [ Repository(repoId: 1, name: "test", fullName: "test", desc: "test", htmlUrl: "test", owner: nil) ]

		let cache = Storage<[StoredRepository]>.inMemory
		try cache.save([ .init(keyword: "test", repositories: mockRepo) ])

		let manager = GithubRepoManager.live(
			loader: .init(request: { _ in
				throw RepositoryError.unauthorized
			}),
			storage: cache
		)

		let result = try await manager.load("test")

		XCTAssertEqual(result, mockRepo)
	}

	func testReplaceExistingCachedDataWithNetworkResult() async throws {

		let mockCacheRepo = [
			Repository(repoId: 1, name: "cache", fullName: "cache", desc: "cache", htmlUrl: "cache", owner: nil)
		]
		let mockNetworkRepo = [
			Repository(repoId: 1, name: "network", fullName: "network", desc: "network", htmlUrl: "network", owner: nil)
		]

		let cache = Storage<[StoredRepository]>.inMemory
		try cache.save([ .init(keyword: "test", repositories: mockCacheRepo) ])

		let manager = GithubRepoManager.live(
			loader: .init(request: { _ in
				SearchRepositoryResponse(
					totalCount: 1,
					incompleteResult: false,
					items: mockNetworkRepo
				)
			}),
			storage: cache
		)

		var cachedRepo = try cache.load()?.first(where: { $0.keyword == "test"}).map(\.repositories)
		XCTAssertEqual(cachedRepo, mockCacheRepo)

		let result = try await manager.load("test")
		XCTAssertEqual(result, mockNetworkRepo)

		cachedRepo = try cache.load()?.first(where: { $0.keyword == "test"}).map(\.repositories)
		XCTAssertEqual(cachedRepo, mockNetworkRepo)
	}

	func testShouldNotTryCachingWhenNetworkResultEmpty() async throws {

		let manager = GithubRepoManager.live(
			loader: .init(request: { _ in
				SearchRepositoryResponse(
					totalCount: 1,
					incompleteResult: false,
					items: []
				)
			}),
			storage: .init(
				load: {
					return nil
				},
				save: { _ in
					XCTFail("Should not be executed")
				},
				removeAllData: {
					XCTFail("Should not be executed")
				}
			)
		)

		_ = try await manager.load("test")
	}

	func testGettingErrorWhenNetworkFailAndCacheIsEmpty() async {

		let manager = GithubRepoManager.live(
			loader: .init(request: { _ in
				throw RepositoryError.unauthorized
			}),
			storage: .init(
				load: {
					return nil
				},
				save: { _ in
					XCTFail("Should not be executed")
				},
				removeAllData: {
					XCTFail("Should not be executed")
				}
			)
		)

		do {
			_ = try await manager.load("test")
			XCTFail("Should return an error")
		} catch {
			XCTAssertNotNil(error as? RepositoryError)
		}
	}
}
