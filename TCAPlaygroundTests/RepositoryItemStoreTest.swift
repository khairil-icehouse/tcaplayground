//
//  RepositoryItemStoreTest.swift
//  TCAPlaygroundTests
//
//  Created by Khairil Ushan on 17/12/21.
//

import ComposableArchitecture
import XCTest
@testable import TCAPlayground

final class RepositoryItemStoreTest: XCTestCase {

	func testEachTapActionShouldBeLogged() {

		var isLogged = false
		let store = TestStore(
			initialState: RepositoryItemState(id: 1, title: "", description: "", ownerName: "", ownerProfilePicture: nil),
			reducer: repositoryItemReducer,
			environment: RepositoryItemEnvironment(log: { _ in
				isLogged = true
				return .none
			})
		)

		store.send(.tapped)

		XCTAssertTrue(isLogged)
	}
}
