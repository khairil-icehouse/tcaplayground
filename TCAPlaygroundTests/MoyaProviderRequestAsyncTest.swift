//
//  MoyaProviderRequestAsyncTest.swift
//  TCAPlaygroundTests
//
//  Created by Khairil Ushan on 18/12/21.
//

import Moya
import XCTest
@testable import TCAPlayground

final class MoyaProviderRequestAsyncTest: XCTestCase {

	func testSuccessfullySerializeResponse() async throws {

		let json = """
		{
		"total_count": 1,
		"incomplete_result": false,
		"items": [
		  {
			"id": 1,
			"name": "test"
		  }
		]
		}
		""".data(using: .utf8)!

		let provider = MoyaProvider<GithubEndpoint>(
			endpointClosure: { _ in
				Endpoint(
					url: "https://api.github.com",
					sampleResponseClosure: {
						.networkResponse(200, json)
					},
					method: .get,
					task: .requestPlain,
					httpHeaderFields: nil
				)
			},
			stubClosure: MoyaProvider.immediatelyStub
		)

		do {
			let response: SearchRepositoryResponse = try await provider.requestAsync(
				.searchRepositories(keyword: ""),
				jsonDecoder: JSONDecoder()
			)
			XCTAssertNotNil(response)
		} catch {
			XCTFail("Fail to serialize response with error: \(error)")
		}
	}

	func testFailToSerializeResponse() async throws {

		let json = """
		{
		"total_count": 1,
		"incomplete_result": false,
		"items": [
		  {
			"id": "1",
			"name": 1
		  }
		]
		}
		""".data(using: .utf8)!

		let provider = MoyaProvider<GithubEndpoint>(
			endpointClosure: { _ in
				Endpoint(
					url: "https://api.github.com",
					sampleResponseClosure: {
						.networkResponse(200, json)
					},
					method: .get,
					task: .requestPlain,
					httpHeaderFields: nil
				)
			},
			stubClosure: MoyaProvider.immediatelyStub
		)

		do {
			_ = try await provider.requestAsync(
				.searchRepositories(keyword: ""),
				jsonDecoder: JSONDecoder()
			) as SearchRepositoryResponse
			XCTFail("Should throwing error")
		} catch {
			switch error as? NetworkError {
			case .jsonSerialization:
				break

			default:
				XCTFail("Invalid error type: \(error)")
			}
		}
	}

	func testGettingNetworkErrorWithServerFailure() async throws {

		let provider = MoyaProvider<GithubEndpoint>(
			endpointClosure: { _ in
				Endpoint(
					url: "https://api.github.com",
					sampleResponseClosure: {
						.networkResponse(500, Data())
					},
					method: .get,
					task: .requestPlain,
					httpHeaderFields: nil
				)
			},
			stubClosure: MoyaProvider.immediatelyStub
		)

		do {
			_ = try await provider.requestAsync(
				.searchRepositories(keyword: ""),
				jsonDecoder: JSONDecoder()
			) as SearchRepositoryResponse
			XCTFail("Should throwing error")
		} catch {
			switch error as? NetworkError {
			case let .underlying(_, code, _, _):
				XCTAssertEqual(code, 500)

			default:
				XCTFail("Invalid error type: \(error)")
			}
		}
	}
}
