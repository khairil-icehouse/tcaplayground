//
//  RepositoryListStoreTest.swift
//  TCAPlaygroundTests
//
//  Created by Khairil Ushan on 15/12/21.
//

import ComposableArchitecture
import XCTest
@testable import TCAPlayground

final class RepositoryListStoreTest: XCTestCase {

	func testSearchRepositoryShouldSuccess() {

		let mockRepo = Repository(
			repoId: 1,
			name: "Test",
			fullName: "Test",
			desc: "Test",
			htmlUrl: "Test",
			owner: nil
		)
		let store = TestStore(
			initialState: RepositoryListState(),
			reducer: repositoryListReducer,
			environment: RepositoryListEnvironment(
				searchRepo: { _ in Effect(value: [ mockRepo ])},
				mainQueue: .immediate
			)
		)

		store.send(.searchTextFieldChanged("iOS")) {
			$0.isLoading = true
			$0.searchQuery = "iOS"
		}

		let expectedRepo = RepositoryItemState(
			id: 1,
			title: "Test",
			description: "Test",
			ownerName: "-",
			ownerProfilePicture: nil
		)
		store.receive(.searchResponse(.success([mockRepo]))) {
			$0.repositories = .init(uniqueElements: [expectedRepo])
			$0.isLoading = false
		}
		store.send(.searchTextFieldChanged("")) {
			$0.searchQuery = ""
			$0.repositories = []
			$0.isLoading = false
		}
	}

	func testSearchRepositoryShouldFail() {

		let store = TestStore(
			initialState: RepositoryListState(),
			reducer: repositoryListReducer,
			environment: RepositoryListEnvironment(
				searchRepo: { _ in Effect(error: RepositoryError.unauthorized) },
				mainQueue: .immediate
			)
		)

		store.send(.searchTextFieldChanged("iOS")) {
			$0.isLoading = true
			$0.searchQuery = "iOS"
		}
		store.receive(.searchResponse(.failure(.unauthorized))) {
			$0.repositories = []
			$0.isLoading = false
		}
		store.send(.searchTextFieldChanged("")) {
			$0.searchQuery = ""
			$0.repositories = []
			$0.isLoading = false
		}
	}
}
